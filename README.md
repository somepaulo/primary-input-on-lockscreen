# Main Layout on Lock Screen

## A GNOME Shell 45+ extension that automatically switches the keyboard layout on the lock screen to the main one.
This is a small, simple extension that solves an annoying issue for users with several different keyboard layouts (until it gets solved upstream).

Whenever the lock screen kicks in, this extension checks for the currently selected keyboard layout and switches it to the main one (the first one in your list of keyboard layouts in Settings).

This is a fork and ESM (GS 45) port of the [Primary Input on LockScreen](https://gitlab.com/sagidayan/primary-input-on-lockscreen) extension by [Sagi](https://gitlab.com/sagidayan). 

#### Installation
[<img src="https://user-images.githubusercontent.com/15643750/212080370-77899e64-bae8-43f1-b67a-fc946785c4b3.png" height="100">](https://extensions.gnome.org/extension/6499/main-layout-on-lock-screen/)

Alternatively, use the [Extension Manager](https://github.com/mjakeman/extension-manager) app.

#### Manual installation
1. Download the extension .zip archive from this repo and unzip it
2. Copy the `main-layout-on-lock-screen@somepaulo.gitlab.com` folder to `~/.local/share/gnome-shell/extensions/`
3. Log out and log back in (on Wayland) or use `Alt+F2`,`r`,`Enter` (on X11)
4. Enable the extension in either `Extensions`, `Extension Manager` or [GNOME Shell Extensions](https://extensions.gnome.org/local/)
